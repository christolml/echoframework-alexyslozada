package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"net/http"
	"os"
)

// me permite generar un archivo con los logs que me registra echo
func main() {

	e := echo.New()

	// se crea el archivo a donde vamos a enviar los logs
	myLog, err := os.OpenFile(
		"logs.log",
		os.O_RDWR | os.O_CREATE | os.O_APPEND,
		0666,
		)

	if err != nil {
		log.Fatal("No se pudo abrir o crear el archivo de logs: %v", err)
	}

	defer myLog.Close()

	// se configura el middleware del log, le decimos que archivo es y el formato que debe de tener
	logConfig := middleware.LoggerConfig{
		Output: myLog,
		//Format: "method=${method}, uri=${uri}, status=${status}\n",
	}

	// se registra el loger
	e.Use(middleware.LoggerWithConfig(logConfig))

	e.GET("/", Index)

	log.Print(e.Start(":7878"))


}



func Index(c echo.Context) error {
	return c.String(http.StatusOK, "Holi mundo")
}