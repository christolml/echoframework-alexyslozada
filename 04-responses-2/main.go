package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func main() {
	ps := make([]Person, 0)
	p := Person{
		FirstName: "Christopher",
		LastName: "Velazquez",
		Age: 21,
	}

	ps = append(ps, p)
	ps = append(ps, p)
	ps = append(ps, p)
	ps = append(ps, p)
	ps = append(ps, p)


	e := echo.New()

	// estoy devolviendo un json a mi peticion
	e.GET("/json", func (c echo.Context) error {
		return c.JSON(http.StatusOK, ps)
	})

	// estoy devolviendo un mi json en formato xml a mi peticion
	e.GET("/xml", func (c echo.Context) error {
		return c.XML(http.StatusOK, ps)
	})


	e.Start(":8080")

}



type Person struct {
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	Age int `json:"age"`
}
