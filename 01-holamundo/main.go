package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func main() {
	// nueva instancia de echo
	e := echo.New()

	// en la ruta / cuando se haga una solicitud GET mando un holi mundo
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Holi mundo")
	})

	// inicia el servidor con el puerto especificado
	e.Start(":8080")
}