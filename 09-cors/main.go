package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//e.Use(middleware.CORS())     cors por default que me permite todos los dominios

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:8989"},
	}))



	e.GET("/data", LoadData)

	e.Start(":8888")
}


func LoadData(c echo.Context) error {
	a := []struct{
		Name string `json:"name"`
	}{
		{"Leidy"},
		{"Chris"},
		{"Trosca"},
		{"Luna"},
	}

	return c.JSON(http.StatusOK, a)
}
