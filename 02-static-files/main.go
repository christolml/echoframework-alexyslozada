package main

import echo2 "github.com/labstack/echo"

func main() {

	e := echo2.New()

/*	sirviendo un archivo nomás, recordar que abajo es lo mismo que decir public/index.html,
    aqui nomas se esta sirviendo el index.html sin el css y el js
    e.File("/", "public")*/

    // con Static estoy sirviendo la carpeta public y por tanto también estoy sirviendo los archivos que estan adentro
    e.Static("/", "public")

	e.Start(":8080")
}
