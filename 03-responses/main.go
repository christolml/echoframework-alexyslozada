package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func main() {

	e := echo.New()

	// en este esta devolviendo un String el cual en content/type: text/plain y lo que escriba se pasara igualito a mi página web
	e.GET("/textoplano", func(c echo.Context) error {
		return c.String(http.StatusOK, "<h1>Hola mundo con String</h1>")
	})

	// en este esta devolviendo en el content-type: text/html y se puede procesar etiquetas html
	e.GET("/html", func(c echo.Context) error {
		return c.HTML(http.StatusOK, `
				<h1>Hola mundo </h1>
				<script>alert('Hola alguien')</script>		
			`)
	})


	e.GET("/nocontent", func(c echo.Context) error {
		return 	c.NoContent(http.StatusOK)
	})

	e.Start(":8080")


}
