package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
)

func main() {

	// instancia de echo
	e :=  echo.New()

	// gracias a este middleware todas las peticiones que se hagan al puerto 80 se redirigen el puerto 443
	e.Pre(middleware.HTTPSRedirect())

	// registramos un handler (index) el cual nos regresa un hola mundo
	e.GET("/", index)

	// cada vez que se haga petición a /divicion me redirige a /division
	e.GET("/divicion", anterior)
	e.GET("/division", nueva)


	// esta go routine me sirve para poder levantar mi servidor tanto en el puerto 80 y el 443
	go func() {
		e.Logger.Fatal(
				e.Start(":80"),
			)
	}()

	// iniciamos el servidor pero con https y le damos los certificados
	e.Logger.Fatal(
		e.StartTLS(":443", "./certificates/cert.pem","./certificates/key.pem"),
		)

}



func index(c echo.Context) error {
	return c.String(http.StatusOK, "Holi mundo")
}

// estoy diciendo que lo mueva permamentemente a la ruta correcta que es /division
func anterior(c echo.Context) error {
	return c.Redirect(http.StatusMovedPermanently,"/division")

}

func nueva(c echo.Context) error {
	return c.String(http.StatusOK, "Hola mundo desde un nuevo link")
}





