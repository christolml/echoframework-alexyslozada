package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func main() {
	e := echo.New()

	// devuelve mi carpeta public y a la vez en index.html ocupa una foto de la carpeta img
	e.Static("/", "public")

	// en esta ruta devuelvo un File
	e.GET("/logoChristolml3/:name", func(c echo.Context) error {
		// es mi parametro de mi ruta de arriba :name
		p := c.Param("name")
		if p == "png" {
			return c.Inline("img/logoChristolml3.png","logoChristolml3.png")
			//return c.File("img/logoChristolml3.png")
		}
		if p == "jpg" {
			return c.Inline("img/machinelearning.png", "machineLearning")
			//return c.File("img/machinelearning.png")
		}

		// con Attachment me permite poner a la disposición de descarga de mi imagen, el segundo parametro es el nombre con el que se va a descargar el archivo
		if p == "att" {
			return c.Attachment("img/logoChristolml3.png", "logoChris.png")
		}
		return c.HTML(http.StatusNotFound,"<p>No encontrado</p>")
	})



	e.Start(":8080")
}
